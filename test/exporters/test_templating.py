"""Unit tests for templating."""

import unittest
from fractions import Fraction
from unittest import mock

from gorps.exporters import templating

from . import make_recipe


class TestRestrictedEval(unittest.TestCase):
    """eval_restricted."""

    def test_builtins_not_accessible(self) -> None:
        with self.assertRaises(NameError):
            templating.eval_restricted(
                "open()", {"__builtins__": {"open": lambda: "opened"}}
            )
        with self.assertRaises(NameError):
            templating.eval_restricted("open()", {})

    def test_example_exploit_not_working(self) -> None:
        """`https://nedbatchelder.com/blog/201206/eval_really_is_dangerous.html`."""
        with self.assertRaises(ValueError):
            templating.eval_restricted(
                """[
    c for c in ().__class__.__base__.__subclasses__()
    if c.__name__ == 'catch_warnings'
][0]()._module.__builtins__""",
                {},
            )

    def test_underscores_not_accessible(self) -> None:
        with self.assertRaises(ValueError):
            templating.eval_restricted(
                "''.__class__",
                {},
            )


class TestTemplating(unittest.TestCase):
    """templating tests."""

    def test_class_selector(self) -> None:
        recipe = make_recipe(Fraction(1))
        with self.subTest(test="wrong attribute"):
            select = templating.class_selector("wrong_attr")
            self.assertRaises(AttributeError)
        with self.subTest(test="wrong attribute / default value"):
            select = templating.class_selector("wrong_attr", default=...)
            self.assertEqual(select(recipe), ...)
        with self.subTest(test="existing attribute"):
            select = templating.class_selector("tags['category'][0]")
            self.assertEqual(select(recipe), "Tequila")

    def test_find_placeholder(self) -> None:
        template = """...
{{recipes.name}}
{{recipes.ingredients}}
"""
        placeholder = next(templating.find_placeholders(template))
        self.assertEqual(placeholder, "recipes.name")

    def test_find_placeholder_deep(self) -> None:
        template = """...
{{ recipes[0].name }}
{{ recipes[1].ingredients[3].amount }}
"""
        marker = next(templating.find_placeholders(template, start=5))
        self.assertEqual(marker, " recipes[1].ingredients[3].amount ")

    def test_find_placeholder_unterminated(self) -> None:
        template = """...
{{recipes.name 
"""  # noqa: W291
        with self.assertRaises(ValueError):
            next(templating.find_placeholders(template))

    def test_find_placeholder_iter(self) -> None:
        template = """...
{{recipes.name}}
{{recipes.ingredients }}
"""
        placeholder = tuple(templating.find_placeholders(template))
        self.assertEqual(placeholder, ("recipes.name", "recipes.ingredients "))

    def test_fill_template(self) -> None:
        template = """
{{recipes[0].title}} - {{recipes[0].tags["category"][0]}}
{{fmt_ingredients(recipes[0].ingredients)}}
{{recipes[0].instruction}}
"""
        expected_output = """
Agavoni - Tequila
INGREDIENTS
Alle Zutaten im Rührglas gut rühren, anschliessend in das vorgekühlte Cocktailglas
seihen und mit Orangenzeste dekorieren.
"""
        recipe = make_recipe(amount=Fraction(1))
        rendered = templating.fill_template(
            template,
            env={
                "recipes": [recipe],
                "fmt_ingredients": mock.Mock(return_value="INGREDIENTS"),
            },
        )
        self.assertEqual(rendered, expected_output)

    def test_format_fraction_returns_decimal_number_for_non_special_fractions(
        self,
    ) -> None:
        self.assertEqual(templating.format_fraction(Fraction(5, 2)), "2.5")
        self.assertEqual(templating.format_fraction(-Fraction(5, 2)), "-2.5")

    def test_format_fraction_returns_special_fraction_for_special_fractions(
        self,
    ) -> None:
        self.assertEqual(templating.format_fraction(Fraction(1, 2)), "½")
        self.assertEqual(templating.format_fraction(Fraction(1, 3)), "⅓")
        self.assertEqual(templating.format_fraction(Fraction(2, 3)), "⅔")
        self.assertEqual(templating.format_fraction(Fraction(3, 4)), "¾")
        self.assertEqual(templating.format_fraction(Fraction(1, 4)), "¼")
        self.assertEqual(templating.format_fraction(-Fraction(1, 2)), "-½")
        self.assertEqual(templating.format_fraction(-Fraction(1, 3)), "-⅓")
        self.assertEqual(templating.format_fraction(-Fraction(2, 3)), "-⅔")
        self.assertEqual(templating.format_fraction(-Fraction(3, 4)), "-¾")
        self.assertEqual(templating.format_fraction(-Fraction(1, 4)), "-¼")

    def test_format_fraction_returns_sum_for_denominator_3(
        self,
    ) -> None:
        self.assertEqual(templating.format_fraction(Fraction(4, 3)), "1+⅓")
        self.assertEqual(templating.format_fraction(-Fraction(4, 3)), "-1.33")
