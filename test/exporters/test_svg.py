"""svg export tests."""

import unittest
from fractions import Fraction
from unittest import mock

from gorps.exporters import svg
from gorps.exporters.templating import FallbackValue

from . import make_recipe


class TestSvg(unittest.TestCase):
    """svg export tests."""

    def test_scan_slot_number(self) -> None:
        template = """...
{{recipes[0].name}}
{{ recipes[0].ingredients}}
{{ recipes[1].ingredients }}
{{recipes[0].ingredients}}
"""
        self.assertEqual(svg.scan_slot_numbers(template, "recipes"), [0, 1])

    def test_scan_slot_number_no_slots(self) -> None:
        template = """...
{{recipes.name}}
{{recipes.ingredients}}
"""
        self.assertEqual(svg.scan_slot_numbers(template, "recipes"), [])

    def test_fill_template_multi_slot(self) -> None:
        template = """
{{recipes[0].title}} - {{recipes[0].tags["category"][0]}}
{{fmt_ingredients(recipes[0].ingredients)}}
{{recipes[0].instruction}}
---
{{recipes[1].title}} - {{recipes[1].tags["category"][0]}}
{{fmt_ingredients(recipes[1].ingredients)}}
{{recipes[1].instruction}}
"""
        single_output = """
Agavoni - Tequila
INGREDIENTS
Alle Zutaten im Rührglas gut rühren, anschliessend in das vorgekühlte Cocktailglas
seihen und mit Orangenzeste dekorieren.
"""
        empty_output = """
 - 


"""  # noqa: W291
        recipes = [make_recipe(amount=Fraction(1))] * 5
        rendered_templates = list(
            svg.fill_template_multi_slot(
                template,
                recipes,
                variables={
                    "fmt_ingredients": mock.Mock(
                        side_effect=lambda x: (
                            "" if isinstance(x, FallbackValue) else "INGREDIENTS"
                        )
                    )
                },
            )
        )
        self.assertEqual(
            rendered_templates,
            [single_output + "---" + single_output] * 2
            + [single_output + "---" + empty_output],
        )


class TestHelpers(unittest.TestCase):
    """helper tests."""

    def test_chunked(self) -> None:
        items = range(10)
        self.assertEqual(
            list(svg.chunked(items, 3)), [[0, 1, 2], [3, 4, 5], [6, 7, 8], [9]]
        )
