"""Utilities for exporter tests."""

from fractions import Fraction

from gorps.model import AmountRange, Ingredient, IngredientGroup, Recipe


def make_recipe(amount: Fraction | AmountRange) -> Recipe:
    return Recipe(
        title="Agavoni",
        instruction="Alle Zutaten im Rührglas gut rühren, anschliessend in das vorgekühlte Cocktailglas\nseihen und mit Orangenzeste dekorieren.",
        amount=Fraction(1),
        ingredients=[
            Ingredient(name="Tequila white", amount=amount, unit="cl"),
            Ingredient(name="Campari", amount=Fraction(3), unit="cl"),
            Ingredient(name="Vermouth rosso", amount=Fraction(3), unit="cl"),
            Ingredient(name="Orange Bitter", amount=Fraction(2), unit="Spritzer"),
            IngredientGroup(
                name="Garnitur",
                ingredients=[Ingredient(name="Orangenzeste", amount=Fraction(1))],
            ),
        ],
        nutrition_labels={},
        tags={
            "Glas": "Cocktail Glass",
            "Zubereitung": "In mixing glass",
            "category": ["Tequila", "Fancy Drinks"],
        },
    )
