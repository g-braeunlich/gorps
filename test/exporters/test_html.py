"""html export tests."""

import unittest
from io import StringIO
from typing import Any, ClassVar

from gorps.exporters import html
from gorps.model import Recipe


class TestProcessHtmlTemplate:
    """Base class for tests."""

    source: str
    environment: ClassVar[dict[str, Any]] = {}
    expected_output: str

    def test(self) -> None:
        processed = StringIO()
        html.process_template(self.source, self.environment, processed)
        unittest.TestCase().assertEqual(
            processed.getvalue(),
            self.expected_output,
        )


class TestSimpleHtml(TestProcessHtmlTemplate, unittest.TestCase):
    """Untemplated html."""

    source = """<!DOCTYPE html>
<html>
  <head>
    <title>Menu Card</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <div/>
    <h1>Menu Card</h1>
    <section>
      <h2 id="food">Food</h2>
      <div class="recipe">
        <h3>Beans</h3>
        Beans, beans &amp; beans!
      </div>
    </section>
  </body>
</html>
"""
    expected_output = """<!DOCTYPE html>
<html>
  <head>
    <title>Menu Card</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <div></div>
    <h1>Menu Card</h1>
    <section>
      <h2 id="food">Food</h2>
      <div class="recipe">
        <h3>Beans</h3>
        Beans, beans &amp; beans!
      </div>
    </section>
  </body>
</html>
"""


class TestTemplatedHtml(TestProcessHtmlTemplate, unittest.TestCase):
    """Templated html."""

    environment: ClassVar[dict[str, Any]] = {
        "title": "Menu Card",
        "groups": {"Beans": [Recipe(title="Beans", instruction="")]},
    }
    source = """<!DOCTYPE html>
<html>
  <head>
    <title>{{ title }}</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <h1>{{ title }}</h1>
    <section v-for="group, recipes in groups.items()">
      <h2 v-bind:id="group">{{ group }}</h2>
      <div class="recipe" v-for="recipe in recipes">
        <h3>{{ recipe.title }}</h3>
      </div>
    </section>
  </body>
</html>
"""
    expected_output = """<!DOCTYPE html>
<html>
  <head>
    <title>Menu Card</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <h1>Menu Card</h1>
    <section>
      <h2 id="Beans">Beans</h2>
      <div class="recipe">
        <h3>Beans</h3>
      </div>
    </section>
  </body>
</html>
"""
