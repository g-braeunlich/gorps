"""exporter tests."""

import unittest
from collections.abc import Iterable
from contextlib import nullcontext
from typing import IO
from unittest import mock

from gorps import exporters
from gorps.exporters.base import TextExporterBase
from gorps.model import Recipe


def make_recipe(title: str, categories: list[str]) -> Recipe:
    return Recipe(
        title=title,
        instruction="...",
        ingredients=[],
        tags={"category": categories} if categories else {},
    )


class TestExporters(unittest.TestCase):
    """exporter tests."""

    def test_group_by(self) -> None:
        a = make_recipe("A", ["a", "b1"])
        b = make_recipe("B", ["b1", "b2"])
        c = make_recipe("C", [])
        recipes = [a, b, c]
        with self.subTest(test="all groups"):
            groups = exporters.group_by(recipes, "tags['category']")
            self.assertEqual(groups, [("a", [a]), ("b1", [a, b]), ("b2", [b])])

        with self.subTest(test="only a, b1 groups"):
            groups = exporters.group_by(
                recipes, "tags['category']", group_names=["a", "b1"]
            )

            self.assertEqual(groups, [("a", [a]), ("b1", [a, b])])
        with self.subTest(test="empty recipes"):
            groups = exporters.group_by([], "tags['category']")
            self.assertEqual(groups, [])

    def test_group_titles(self) -> None:
        a = make_recipe("A", [])
        b = make_recipe("B", [])
        b1 = make_recipe("B1", [])
        c = make_recipe("C", [])
        recipes = [a, b, b1, c]
        with self.subTest(test="no groups"):
            groups = exporters.group_titles(recipes, [])
            self.assertEqual(groups, [])

        with self.subTest(test=""):
            groups = exporters.group_titles(
                recipes,
                [{"name": "a", "titles": ["A"]}, {"name": "b", "titles": ["B", "B1"]}],
            )
            self.assertEqual(groups, [("a", [a]), ("b", [b, b1])])
        with self.subTest(test="missing title"), self.assertRaises(ValueError):
            exporters.group_titles(
                recipes,
                [{"name": "a", "titles": ["A"]}, {"name": "d", "titles": ["D"]}],
            )

    @staticmethod
    def test_export_multifile() -> None:
        a = make_recipe("A", ["a", "b1"])
        a2 = make_recipe("A", ["b1", "b2"])
        mock_export_stream = mock.Mock()

        class TestExporter(TextExporterBase):
            ext = "test"

            def export_stream(self, recipes: Iterable[Recipe], stream: IO[str]) -> None:
                mock_export_stream(recipes, stream)

        exporter = TestExporter()
        with (
            mock.patch("builtins.open", lambda path, *_, **__: nullcontext(path)),
            mock.patch("os.makedirs"),
        ):
            exporter.export_multifile(recipes=[a, a2], out_dir="/out/")
        mock_export_stream.assert_has_calls(
            [mock.call([a], "/out/a.test"), mock.call([a2], "/out/a-2.test")]
        )
