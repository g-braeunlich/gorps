"""Example recipe for tests."""

from datetime import timedelta
from fractions import Fraction

from gorps.model import Ingredient, Recipe

RECIPE = Recipe(
    title="Beans with Bacon a la Bud Spencer",
    description="Chuck Norris? Never heard about her!",
    instruction="""Finely dice the onion and briefly sauté in hot oil together with the bacon and garlic.

Add cabanossi / salami.

Add beans and strained tomatoes.

Season with thyme, chili, pepper, paprika and salt.

Serve with a wooden spoon.""",
    amount=Fraction(1),
    amount_unit="Pan",
    preparation_time=timedelta(seconds=900),
    cooking_time=timedelta(seconds=600),
    cookware=["Pan", "Wooden spoon"],
    source="https://www.kabeleins.ch/sosiehtsaus/essen-trinken/rezepte/bohnen-mit-speck-nach-bud-spencer",
    link="https://www.kabeleins.ch/sosiehtsaus/essen-trinken/rezepte/bohnen-mit-speck-nach-bud-spencer",
    notes="Bud gives a damn about cream, but if you prefer, serve with cream!",
    tags={"category": ["Main courses"]},
    ingredients=[
        Ingredient(name="Finely diced bacon", amount=Fraction(125), unit="g"),
        Ingredient(
            name="Clove of garlic",
            amount=Fraction(1),
        ),
        Ingredient(
            name="Salami or Cabanossi",
            amount=Fraction(150),
            unit="g",
        ),
        Ingredient(
            name="Onion",
            amount=Fraction(1),
        ),
        Ingredient(name="Strained tomatoes", amount=Fraction(250), unit="ml"),
        Ingredient(
            name="White beans",
            amount=Fraction(1),
            unit="Can",
        ),
        Ingredient(
            name="Kidney beans",
            amount=Fraction(1),
            unit="Can",
        ),
        Ingredient(
            name="Thyme",
            amount=Fraction(1),
            unit="tsp",
        ),
        Ingredient(
            name="Pepper",
            amount=Fraction(1),
            unit="tsp",
        ),
        Ingredient(
            name="Chili powder",
            amount=Fraction(1),
            unit="tsp",
        ),
        Ingredient(
            name="Paprika powder",
            amount=Fraction(1),
            unit="tsp",
        ),
        Ingredient(
            name="Salt",
            amount=Fraction(1),
            unit="Pinch",
        ),
        Ingredient(
            name="Heat-resistant oil (rapeseed oil or olive oil)",
            amount=Fraction(1),
            unit="Shot",
        ),
        Ingredient(name="Honey", amount=Fraction(2), unit="tbsp"),
    ],
)
