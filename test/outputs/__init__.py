"""Example input for tests."""

# ruff: noqa: S108

import os

from gorps.exporters.templating import load_text_file

from . import svg as _svg

DIR = os.path.dirname(__file__)

md = {
    "/tmp/out/" + f: load_text_file(os.path.join(DIR, f))
    for f in ("beans-with-bacon-a-la-bud-spencer.md", "more-beans.md")
}
fo_xml = load_text_file(os.path.join(DIR, "menucard.fo.xml"))
svg = _svg.content
openrecipes_xml = {
    "/tmp/out/" + f: load_text_file(os.path.join(DIR, f))
    for f in ("beans-with-bacon-a-la-bud-spencer.openrecipes", "more-beans.openrecipes")
}
openrecipes_sql = load_text_file(os.path.join(DIR, "openrecipes.sql")).rstrip()
yml = load_text_file(os.path.join(DIR, "more-beans.yml"))
png = b"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x01\x00\x00\x00\x01\x08\x02\x00\x00\x00\x90wS\xde\x00\x00\x00\x0cIDAT\x08\xd7c\xf8\xff\xff?\x00\x05\xfe\x02\xfe\xdc\xccY\xe7\x00\x00\x00\x00IEND\xaeB`\x82"
html_beans = load_text_file(os.path.join(DIR, "beans.html"))
html_more_beans = load_text_file(os.path.join(DIR, "more-beans.html"))
html_menucard = load_text_file(os.path.join(DIR, "menucard.html"))
