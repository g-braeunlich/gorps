# Beans with Bacon a la Bud Spencer

* 125 g Finely diced bacon
* 1 Clove of garlic
* 150 g Salami or Cabanossi
* 1 Onion
* 250 ml Strained tomatoes
* 1 Can White beans
* 1 Can Kidney beans
* 1 tsp Thyme
* 1 tsp Pepper
* 1 tsp Chili powder
* 1 tsp Paprika powder
* 1 Pinch Salt
* 1 Shot Heat-resistant oil (rapeseed oil or olive oil)
* 2 tbsp Honey

Finely dice the onion and briefly sauté in hot oil together
with the bacon and garlic.

Add cabanossi / salami.

Add beans and strained tomatoes.

Season with thyme, chili, pepper, paprika and salt.

