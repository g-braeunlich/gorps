"""Yml import unit tests."""

import unittest
from fractions import Fraction
from io import StringIO

from gorps.importers import yml
from gorps.model import AmountRange
from test.exporters import make_recipe
from test.exporters.test_yml import expected_yml as source_yml


class TestYmlImport(unittest.TestCase):
    """yml import tests."""

    def test_import(self) -> None:
        with self.subTest("scalar amount"):
            buf = StringIO(source_yml.replace("{{ amount }}", " 2.0"))
            recipe = yml.load_recipe_from_stream(buf)
            self.assertEqual(recipe, make_recipe(Fraction(2)))
        with self.subTest("range amount"):
            buf = StringIO(
                source_yml.replace("{{ amount }}", "\n    min: 2.0\n    max: 3.0")
            )
            recipe = yml.load_recipe_from_stream(buf)
            self.assertEqual(recipe, make_recipe(AmountRange(Fraction(2), Fraction(3))))
