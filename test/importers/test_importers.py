"""Unit tests for the importers module."""

import unittest
from fractions import Fraction

from gorps import importers
from gorps.model import Ingredient, Recipe


class TestFilter(unittest.TestCase):
    """Filter unit tests."""

    def setUp(self) -> None:
        self.recipes = [
            Recipe(
                title="A",
                instruction="",
                ingredients=[
                    Ingredient(name="Campari", amount=Fraction(3), unit="cl"),
                    Ingredient(name="Ice"),
                ],
            ),
            Recipe(
                title="B",
                instruction="",
                ingredients=[
                    Ingredient(name="Ice Cream", amount=Fraction(300), unit="g"),
                    Ingredient(name="Ice"),
                ],
            ),
            Recipe(
                title="C",
                instruction="",
                ingredients=[
                    Ingredient(name="Beans", amount=Fraction(300), unit="g"),
                ],
            ),
        ]

    def test_filter_ingredients(self) -> None:
        filtered_recipes = list(
            importers.filter_ingredients(self.recipes, frozenset(("Ice",)))
        )
        expected_recipes = [
            Recipe(
                title="A",
                instruction="",
                ingredients=[
                    Ingredient(name="Campari", amount=Fraction(3), unit="cl"),
                ],
            ),
            Recipe(
                title="B",
                instruction="",
                ingredients=[
                    Ingredient(name="Ice Cream", amount=Fraction(300), unit="g"),
                ],
            ),
            Recipe(
                title="C",
                instruction="",
                ingredients=[
                    Ingredient(name="Beans", amount=Fraction(300), unit="g"),
                ],
            ),
        ]
        self.assertEqual(filtered_recipes, expected_recipes)
